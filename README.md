Opdracht 2B

- Change method signature					Auto.java regel 129
- Extract method							Auto.java regel 129
- Extract method object						databaseController.java regel 568
- Extract parameter object					Auto.java regel 77
- Extract constant/field/variable			Auto.java regel 149		
- Inline									databaseController regel 55
- Extract interface/abstract pull up/down	InterfacedatabaseController.java
- Remove middleman							Klus.java regel 83 tot 104
- Introduce factory method					Factuur.java regel 29
- Introduce builder							Auto.java regel 68
- Move refactoring							brievenService.java in Package applicatie2
- Replace inheritance with delegation		AP1.java en AP2.java
- Make static/instance						Brandstof.java regel 211