Uitleg opdracht 2A:

Instanties van klasse Klus kunnen nu geïnstantieerd worden via de KlusBuilder.
Dit is gedaan door te refactoren.

Voorbeelden van het aanroepen van de Builder zitten in klasse NewKlus op regel 249 en in databaseController op regel 335.

Rick Ossendrijver.